package com.barcena;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        System.out.println("---Activity 3---");

        // Instances
        User a = new User("John", "Doe", 123);

        User b = new User("Jane", "Fox", 456);

        // Magic Code
        List<User> users = new ArrayList<>();

        users.add(0,a);
        users.add(1,b);

        for (User user : users) {
            user.display();
        }
    }




}
