package com.barcena;

public class User {

    // Properties
    private String firstName;
    private String lastName;
    private int contactNumber;

    public User() {
        System.out.println("I am a parameter");
    }

    public User(String newFirstName, String newLastName, int newContactNumber) {
        System.out.println("New user created");
        this.firstName = newFirstName;
        this.lastName = newLastName;
        this.contactNumber = newContactNumber;

    }

    // Getters
    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public int getContactNumber() {
        return this.contactNumber;
    }

    // Setters

    public void setFirstName(String newFirstName) {
        this.firstName = newFirstName;
    }

    public void setLastName(String newLastName) {
        this.lastName = newLastName;
    }

    public void setContactNumber(int newContactNumber) {
        this.contactNumber = newContactNumber;
    }

    // Method

    public void display() {
        System.out.println("Hi " + firstName + " " + lastName + ", Your number is " + contactNumber);
    }
}
